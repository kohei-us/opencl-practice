#ifndef INCLUDED_UTIL_HPP
#define INCLUDED_UTIL_HPP

#include <memory>
#include <functional>

class time_tracker
{
    struct impl;
    std::unique_ptr<impl> mp_impl;
public:
    time_tracker();
    ~time_tracker();

    double get_duration() const;
};

class scope_end_executor
{
    std::function<void()> m_func;
public:
    scope_end_executor(std::function<void()> func);
    ~scope_end_executor();
};

#endif
