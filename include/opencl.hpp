#ifndef INCLUDEED_OPENCL_HPP
#define INCLUDEED_OPENCL_HPP

#define CHECK_STATUS_OR_THROW(status, msg) \
    if (status != CL_SUCCESS) \
    { \
        std::ostringstream os; \
        os << msg << " (status=" << status << ")"; \
        throw std::runtime_error(os.str()); \
    }

#ifndef CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#endif

#ifndef CL_USE_DEPRECATED_OPENCL_2_0_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#endif

#include <CL/cl.h>
#include <memory>
#include <string>

struct platform_device_t
{
    cl_platform_id platform;
    cl_device_id device;
};

/**
 * Query available devices and have the user pick a device.
 *
 * @return the platform and device pair that the user has picked.
 */
platform_device_t pick_device();

/**
 * Represents a single OpenCL program instance.
 */
class program_module
{
    struct impl;
    std::unique_ptr<impl> mp_impl;
public:
    program_module() = delete;
    program_module(const program_module&) = delete;
    program_module(program_module&&) = delete;

    program_module(cl_device_id device, cl_context cxt, const char* src, size_t src_n);
    ~program_module();

    cl_kernel get_kernel(const std::string& name);

    size_t get_max_work_group_size() const;
    size_t get_max_work_group_size(cl_kernel kernel) const;
};

#endif
