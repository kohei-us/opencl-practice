#include "util.hpp"

#include <chrono>
#include <memory>

struct time_tracker::impl
{
    std::chrono::high_resolution_clock::time_point m_start_time;

    impl() : m_start_time(get_time()) {}

    std::chrono::high_resolution_clock::time_point get_time() const
    {
        return std::chrono::high_resolution_clock::now();
    }
};

time_tracker::time_tracker() : mp_impl(std::make_unique<impl>()) {}
time_tracker::~time_tracker() {}

double time_tracker::get_duration() const
{
    std::chrono::duration<double, std::milli> d = mp_impl->get_time() - mp_impl->m_start_time;
    return d.count() / 1000.0;
}

scope_end_executor::scope_end_executor(std::function<void()> func) :
    m_func(std::move(func)) {}

scope_end_executor::~scope_end_executor()
{
    m_func();
}
