/*************************************************************************
 *
 * Copyright (c) 2017 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include <CL/cl.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>

#define CHECK_STATUS_OR_EXIT(status, msg) \
    if (status != CL_SUCCESS) \
    { \
        std::cerr << msg << std::endl; \
        return EXIT_FAILURE; \
    }

using platform_info_def_t = std::pair<cl_platform_info, const char*>;
using device_info_def_t = std::pair<cl_device_info, const char*>;

const std::vector<platform_info_def_t> platform_infos = {
    { CL_PLATFORM_NAME, "platform name" },
    { CL_PLATFORM_PROFILE, "profile" },
    { CL_PLATFORM_VERSION, "version" },
    { CL_PLATFORM_VENDOR, "vendor" },
    { CL_PLATFORM_EXTENSIONS, "extensions" },
};

const std::vector<device_info_def_t> device_info_strings = {
    { CL_DEVICE_NAME, "name" },
    { CL_DEVICE_PROFILE, "profile" },
    { CL_DEVICE_VENDOR, "vendor" },
    { CL_DEVICE_VERSION, "device version" },
    { CL_DRIVER_VERSION, "driver version" },
    { CL_DEVICE_EXTENSIONS, "extensions" },
};

const std::vector<device_info_def_t> device_info_uints = {
    { CL_DEVICE_MAX_COMPUTE_UNITS, "max compute units" },
    { CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, "global memory cacheline size" },
    { CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, "preferred width for char" },
    { CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, "preferred width for short" },
    { CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, "preferred width for int" },
    { CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, "preferred width for long" },
    { CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, "preferred width for float" },
    { CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, "preferred width for double" },
};

const std::vector<std::pair<cl_device_info, const char*>> device_info_ulongs = {
    { CL_DEVICE_GLOBAL_MEM_SIZE, "global memory size (in bytes)" },
    { CL_DEVICE_LOCAL_MEM_SIZE, "local memory size (in bytes)" },
};

template<typename T>
void print_info(cl_device_id did, const std::vector<device_info_def_t>& def)
{
    for (const auto& di : def)
    {
        T v = 0;
        cl_int status = clGetDeviceInfo(did, di.first, sizeof(T), &v, nullptr);
        std::cout << "  * " << di.second << ": ";
        if (status == CL_SUCCESS)
            std::cout << v;
        else
            std::cout << "-";
        std::cout << std::endl;
    }
}

template<>
void print_info<std::string>(cl_device_id did, const std::vector<device_info_def_t>& def)
{
    std::string buf(255, '\0');

    for (const auto& di : def)
    {
        size_t n;
        cl_int status = clGetDeviceInfo(did, di.first, buf.size(), (void*)buf.data(), &n);
        std::cout << "  * " << di.second << ": ";
        if (status == CL_SUCCESS)
            std::cout << buf.data();
        else
            std::cout << "-";
        std::cout << std::endl;
    }
}

void print_wi_info(cl_device_id did)
{
    cl_uint wi_dims = 0;
    cl_int status = clGetDeviceInfo(did, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &wi_dims, nullptr);
    if (status != CL_SUCCESS)
        return;

    std::cout << "  * max work item dimensions: " << wi_dims << std::endl;
    std::vector<size_t> max_wi(wi_dims, 0u);
    status = clGetDeviceInfo(did, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t)*max_wi.size(), max_wi.data(), nullptr);
    if (status != CL_SUCCESS)
        return;

    for (size_t i = 0; i < max_wi.size(); ++i)
        std::cout << "  * max work item " << i << ": " << max_wi[i] << std::endl;
}

int main()
{
    cl_uint platformCount = 0;
    cl_int status;

    status = clGetPlatformIDs(0, nullptr, &platformCount);
    CHECK_STATUS_OR_EXIT(status, "failed to obtain the platform ID count.")

    std::vector<cl_platform_id> platformIDs(platformCount);
    status = clGetPlatformIDs(platformCount, platformIDs.data(), nullptr);
    CHECK_STATUS_OR_EXIT(status, "failed to obtains a list of platform IDs.")

    for (const cl_platform_id pid : platformIDs)
    {
        std::cout << "==" << std::endl;

        std::string buf(255, '\0');
        for (const auto& platformInfo : platform_infos)
        {
            size_t n;
            status = clGetPlatformInfo(pid, platformInfo.first, buf.size(), (void*)buf.data(), &n);
            if (status == CL_SUCCESS)
                std::cout << "* " << platformInfo.second << ": " << buf << std::endl;
        }

        cl_uint deviceCount = 0;
        status = clGetDeviceIDs(pid, CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceCount);
        CHECK_STATUS_OR_EXIT(status, "failed to obtain the device ID count.")

        std::vector<cl_device_id> deviceIDs(deviceCount);
        status = clGetDeviceIDs(pid, CL_DEVICE_TYPE_ALL, deviceCount, deviceIDs.data(), nullptr);
        CHECK_STATUS_OR_EXIT(status, "failed to obtain a list of device IDs.")

        for (const cl_device_id did : deviceIDs)
        {
            std::cout << "--" << std::endl;

            print_info<std::string>(did, device_info_strings);
            print_info<cl_uint>(did, device_info_uints);
            print_info<cl_ulong>(did, device_info_ulongs);
            print_wi_info(did);
        }
    }

    return EXIT_SUCCESS;
}

