/*************************************************************************
 *
 * Copyright (c) 2017 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <cassert>
#include <cstring>
#include <sstream>
#include <random>
#include <numeric>

#include "opencl.hpp"
#include "util.hpp"

const char* ks_sum =

#include "kernel/sum.cl.inl"

;

size_t ks_sum_n = std::strlen(ks_sum);

void run_sum(const platform_device_t& pd, size_t wg_count)
{
    cl_context cxt = nullptr;
    cl_command_queue cq = nullptr;
    cl_mem arg1 = nullptr;
    cl_mem arg3 = nullptr;

    scope_end_executor se_exec(
        [&]()
        {
            if (arg1)
                clReleaseMemObject(arg1);

            if (arg3)
                clReleaseMemObject(arg3);

            if (cq)
                clReleaseCommandQueue(cq);

            if (cxt)
                clReleaseContext(cxt);
        }
    );

    const cl_context_properties cxt_props[] = {
        CL_CONTEXT_PLATFORM, reinterpret_cast<cl_context_properties>(pd.platform),
        0
    };

    cl_int status = CL_SUCCESS;
    cxt = clCreateContext(cxt_props, 1, &pd.device, nullptr, nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create context.")

    // use deprecated function here to avoid crash.
    cq = clCreateCommandQueue(cxt, pd.device, 0, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create command queue")

    program_module pm(pd.device, cxt, ks_sum, ks_sum_n);
    cl_kernel ksum = pm.get_kernel("sum");
    if (!ksum)
        throw std::runtime_error("failed to create kernel for 'sum");

    size_t max_wg_size = pm.get_max_work_group_size(ksum);

    std::cout << "max work group size: " << max_wg_size << std::endl;
    std::cout << "number of work groups: " << wg_count << std::endl;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dist(1.0f, 200.0f);

    std::vector<float> values;
    values.reserve(max_wg_size*wg_count);
    for (size_t i = 0; i < max_wg_size*wg_count; ++i)
        values.push_back(dist(gen));

    std::cout << "number of values: " << values.size() << std::endl;
    float sum_cpu = std::accumulate(values.begin(), values.end(), 0.0f);
    std::cout << "sum (cpu) = " << sum_cpu << std::endl;

    std::vector<float> output(wg_count, 0.0f);

    arg1 = clCreateBuffer(cxt, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*values.size(), values.data(), &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg1.")
    arg3 = clCreateBuffer(cxt, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float)*output.size(), output.data(), &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg3.")

    status = clSetKernelArg(ksum, 0, sizeof(arg1), &arg1);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 1.")

    // 2nd arg is a local memory. The kernel needs to allocate a float array
    // whose size equals that of the values.
    status = clSetKernelArg(ksum, 1, sizeof(float)*max_wg_size, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 2.")

    status = clSetKernelArg(ksum, 2, sizeof(arg3), &arg3);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 3.")

    // Dispatch the kernel.
    size_t wi = values.size();
    size_t wg = max_wg_size;
    status = clEnqueueNDRangeKernel(cq, ksum, 1, nullptr, &wi, &wg, 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to disptach the kernel.")

    status = clFlush(cq);
    CHECK_STATUS_OR_THROW(status, "failed to flush the command queue.")

    status = clEnqueueReadBuffer(cq, arg3, CL_TRUE, 0, sizeof(float)*output.size(), output.data(), 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to read buffer from the kernel.")

    float final_sum = std::accumulate(output.begin(), output.end(), 0.0f);
    std::cout << "sum = " << final_sum << std::endl;
}

int main()
{
    try
    {
        platform_device_t device_selected = pick_device();
        run_sum(device_selected, 5);
    }
    catch (const std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
