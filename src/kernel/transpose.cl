/**
 * Transpose a square matrix.
 *
 * @param g_mat input array consisting of matrix element values.
 * @param size column size of the matrix, which is the original column size
 *             divided by 4 since this kernel uses float4.
 */
__kernel void transpose_f4(__global float4* g_mat, uint size)
{
    float4 l_mat[8];

    __global float4 *src, *dst;

    // Find the correct (row, column) position for the work item.
    int col = get_global_id(0);
    int row = 0;
    while (col >= size)
    {
        col -= size;
        --size;
        ++row;
    }

    // Reset the col and size values to their original ones.
    col += row;
    size += row;

#ifdef DEBUG_KERNEL
    printf("wi: %2d; wg: %2d; col: %2d, row: %2d, size: %2d\n",
           get_global_id(0), get_local_id(0), col, row, size);
#endif

    src = g_mat + row * size * 4 + col;
    l_mat[0] = src[0];
    l_mat[1] = src[size];
    l_mat[2] = src[2*size];
    l_mat[3] = src[3*size];

    if (row == col)
    {
        src[0]      = (float4)(l_mat[0].x, l_mat[1].x, l_mat[2].x, l_mat[3].x);
        src[size]   = (float4)(l_mat[0].y, l_mat[1].y, l_mat[2].y, l_mat[3].y);
        src[2*size] = (float4)(l_mat[0].z, l_mat[1].z, l_mat[2].z, l_mat[3].z);
        src[3*size] = (float4)(l_mat[0].w, l_mat[1].w, l_mat[2].w, l_mat[3].w);
    }
    else if (row < col)
    {
        dst = g_mat + col * size * 4 + row;
        l_mat[4] = dst[0];
        l_mat[5] = dst[size];
        l_mat[6] = dst[2*size];
        l_mat[7] = dst[3*size];

        dst[0]      = (float4)(l_mat[0].x, l_mat[1].x, l_mat[2].x, l_mat[3].x);
        dst[size]   = (float4)(l_mat[0].y, l_mat[1].y, l_mat[2].y, l_mat[3].y);
        dst[2*size] = (float4)(l_mat[0].z, l_mat[1].z, l_mat[2].z, l_mat[3].z);
        dst[3*size] = (float4)(l_mat[0].w, l_mat[1].w, l_mat[2].w, l_mat[3].w);

        src[0]      = (float4)(l_mat[4].x, l_mat[5].x, l_mat[6].x, l_mat[7].x);
        src[size]   = (float4)(l_mat[4].y, l_mat[5].y, l_mat[6].y, l_mat[7].y);
        src[2*size] = (float4)(l_mat[4].z, l_mat[5].z, l_mat[6].z, l_mat[7].z);
        src[3*size] = (float4)(l_mat[4].w, l_mat[5].w, l_mat[6].w, l_mat[7].w);
    }
}

__kernel void transpose_f1(__global float* g_mat, uint size)
{
    __global float *src, *dst;

    // Find the correct (row, column) position for the work item.
    int col = get_global_id(0);
    int row = 0;
    while (col >= size)
    {
        col -= size;
        --size;
        ++row;
    }

    // Reset the col and size values to their original ones.
    col += row;
    size += row;

#ifdef DEBUG_KERNEL
    printf("wi: %2d; wg: %2d; col: %2d, row: %2d, size: %2d\n",
           get_global_id(0), get_local_id(0), col, row, size);
#endif

    float pv_src; // private value.

    src = g_mat + row * size + col;
    pv_src = *src;

    if (row == col)
    {
        *src = pv_src;
    }
    else if (row < col)
    {
        dst = g_mat + col * size + row;
        float pv_dst = *dst;

        *dst = pv_src;
        *src = pv_dst;
    }
}

