
__kernel void radix_sort_uint16_count(
    __global const ushort* data, __local uint* counter, __private uchar byte_pos, __private ushort mask,
    __global uint* gcounter)
{
    const size_t lsize = get_local_size(0);
    const size_t lid = get_local_id(0);

    // Initialize the local counter array to zero.
    for (size_t i = lid; i < 256; i += lsize)
        counter[i] = 0;

    barrier(CLK_LOCAL_MEM_FENCE);

    ushort v = data[get_global_id(0)];
    ushort bv = (v & mask);
    bv >>= 8*byte_pos;
    atomic_inc(&counter[bv]);

    barrier(CLK_LOCAL_MEM_FENCE);

    // Copy the local counter to the global one.
    const size_t gid = get_group_id(0);
    for (size_t i = lid; i < 256; i += lsize)
    {
        size_t pos = gid * 256 + i;
        gcounter[pos] = counter[i];
    }
}

__kernel void radix_sort_uint16_mem_offsets(
    __global const uint* gcounter, __private uint gcount, __global uint* mem_offsets)
{
    uint current_offset = 0;

    for (size_t byte_pos = 0; byte_pos < 256; ++byte_pos)
    {
        for (uint gid = 0; gid < gcount; ++gid)
        {
            size_t pos = gid * 256 + byte_pos;
            mem_offsets[pos] = current_offset;
            current_offset += gcounter[pos];
        }
    }
}

/**
 * The number of deployed work items of this kernel must equal the number of
 * segments of the input data.  Each work item handles the entire segment of
 * the data.
 *
 * @param segment_size length of each data segment.
 */
__kernel void radix_sort_uint16_sorted_pos(
    __global const ushort* data, __global uint* mem_offsets,
    __private const uint segment_size, __private const uchar byte_pos, __private const ushort mask,
    __global ushort* sorted)
{
    const size_t id = get_global_id(0);
    const size_t data_size = get_global_size(0) * segment_size;
    const uint segment_offset = segment_size * id;

    for (uint i = 0; i < segment_size; ++i)
    {
        ushort v = data[segment_offset+i];
        ushort bv = (v & mask);
        bv >>= 8*byte_pos;

        size_t pos = id * 256 + bv;
        uint mem_offset = mem_offsets[pos]++;
        sorted[mem_offset] = v;
    }
}
