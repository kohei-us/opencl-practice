
/**
 * Find a pattern inside text.
 *
 * @param pattern text pattern to look for.
 * @param text text to search in.
 * @param chars_per_item number of characters each item handles.
 * @param local_result local memory to store workgroup results.
 * @param global_result global memory to store all results.
 */
__kernel void string_search(
    char16 pattern, __global char* text, int chars_per_item, __local int* local_result, __global int* global_result)
{
    char16 text_vector, check_vector;

    local_result[0] = 0;
    local_result[1] = 0;
    local_result[2] = 0;
    local_result[3] = 0;

    barrier(CLK_LOCAL_MEM_FENCE);

    // text offset for this work item.
    int item_offset = get_global_id(0) * chars_per_item;

    // Keep sliding the range by one character.
    for (int i = item_offset; i < item_offset + chars_per_item; ++i)
    {
        text_vector = vload16(0, text + i);
        check_vector = text_vector == pattern;

        if (all(check_vector.s0123)) // first 4 chars
            atomic_inc(local_result);
        if (all(check_vector.s4567)) // next 4 chars
            atomic_inc(local_result+1);
        if (all(check_vector.s89AB)) // next 4 chars
            atomic_inc(local_result+2);
        if (all(check_vector.sCDEF)) // next 4 chars
            atomic_inc(local_result+3);
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    if (get_local_id(0) == 0)
    {
        atomic_add(global_result,     local_result[0]);
        atomic_add(global_result + 1, local_result[1]);
        atomic_add(global_result + 2, local_result[2]);
        atomic_add(global_result + 3, local_result[3]);
    }
}
