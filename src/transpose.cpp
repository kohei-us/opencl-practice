/*************************************************************************
 *
 * Copyright (c) 2017 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <iterator>
#include <cmath>
#include <iomanip>

#include "opencl.hpp"
#include "util.hpp"

const char* ks_transpose =

#include "kernel/transpose.cl.inl"

;

size_t ks_transpose_n = std::strlen(ks_transpose);

cl_uint get_matrix_size(const std::vector<float>& mxdata)
{
    cl_uint mxsize = static_cast<cl_uint>(std::sqrt(mxdata.size()));

    if (mxsize % 4)
        throw std::runtime_error("matrix size must be a multiple of 4.");

    if (mxsize*mxsize != mxdata.size())
        throw std::runtime_error("invalid matrix size.");

    return mxsize;
}

std::vector<cl_float4> to_float4(const std::vector<float>& mxdata)
{
    std::vector<cl_float4> values;
    size_t n = mxdata.size() / 4;
    values.reserve(n);
    auto it = mxdata.begin();

    for (size_t i = 0; i < n; ++i)
    {
        cl_float4 vs = {
            *it++, *it++, *it++, *it++
        };
        values.emplace_back(vs);
    }

    return values;
}

void print_element(std::ostream& os, const cl_float v)
{
    char bs =  ' '; // blank space
    os << std::setfill(bs) << std::setw(5) << v << bs;
}

void print_matrix(const std::vector<cl_float4>& values, cl_uint mx_size)
{
    size_t col = 0;
    size_t row = 0;
    size_t col_printed = 0;
    size_t max_size = 16;
    std::for_each(values.begin(), values.end(),
        [&](const cl_float4& v)
        {
            ++col;
            bool print = col <= max_size/4 && row < max_size;

            if (print)
            {
                ++col_printed;
                print_element(std::cout, v.s[0]);
                print_element(std::cout, v.s[1]);
                print_element(std::cout, v.s[2]);
                print_element(std::cout, v.s[3]);
            }

            if (col >= mx_size/4)
            {
                if (col_printed)
                    std::cout << std::endl;
                col = 0;
                col_printed = 0;
                ++row;
            }
        }
    );
}

void print_matrix(const std::vector<float>& values, cl_uint mx_size)
{
    size_t col = 0;
    size_t row = 0;
    size_t col_printed = 0;
    size_t max_size = 16;
    std::for_each(values.begin(), values.end(),
        [&](const float& v)
        {
            ++col;
            bool print = col <= max_size && row < max_size;

            if (print)
            {
                ++col_printed;
                print_element(std::cout, v);
            }

            if (col >= mx_size)
            {
                if (col_printed)
                    std::cout << std::endl;
                col = 0;
                col_printed = 0;
                ++row;
            }
        }
    );
}

void run_transpose_f4(const platform_device_t& pd, const std::vector<float>& mxdata)
{
    cl_context cxt = nullptr;
    cl_command_queue cq = nullptr;
    cl_mem arg1 = nullptr;
    cl_mem arg3 = nullptr;

    scope_end_executor se_exec(
        [&]()
        {
            if (arg1)
                clReleaseMemObject(arg1);

            if (arg3)
                clReleaseMemObject(arg3);

            if (cq)
                clReleaseCommandQueue(cq);

            if (cxt)
                clReleaseContext(cxt);
        }
    );

    const cl_context_properties cxt_props[] = {
        CL_CONTEXT_PLATFORM, reinterpret_cast<cl_context_properties>(pd.platform),
        0
    };

    cl_int status = CL_SUCCESS;
    cxt = clCreateContext(cxt_props, 1, &pd.device, nullptr, nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create context.")

    // use deprecated function here to avoid crash.
    cq = clCreateCommandQueue(cxt, pd.device, 0, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create command queue")

    program_module pm(pd.device, cxt, ks_transpose, ks_transpose_n);
    cl_kernel ktranspose = pm.get_kernel("transpose_f4");
    if (!ktranspose)
        throw std::runtime_error("failed to create kernel for 'transpose");

    size_t max_wg_size = pm.get_max_work_group_size(ktranspose);
    cl_uint mx_size = get_matrix_size(mxdata);
    cl_uint mx_block_size = mx_size / 4; // We use cl_float4.
    size_t wi_size = mx_block_size * mx_block_size;
    std::vector<cl_float4> values = to_float4(mxdata);
    size_t wg_size = std::min(max_wg_size, wi_size);

    std::cout << "matrix size: " << mx_size << " x " << mx_size << std::endl;
    std::cout << "max work group size: " << max_wg_size << std::endl;

    std::cout << "number of values: " << values.size() << std::endl;

    print_matrix(values, mx_size);

    arg1 = clCreateBuffer(cxt, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_float4)*values.size(), values.data(), &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg1.")

    status = clSetKernelArg(ktranspose, 0, sizeof(arg1), &arg1);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 1.")

    status = clSetKernelArg(ktranspose, 1, sizeof(mx_size), &mx_block_size);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 2.")

    // Dispatch the kernel.
    std::cout << "work item size: " << wi_size << "; work group size: " << wg_size << std::endl;

    status = clEnqueueNDRangeKernel(cq, ktranspose, 1, nullptr, &wi_size, &wg_size, 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to disptach the kernel.")

    status = clFlush(cq);
    CHECK_STATUS_OR_THROW(status, "failed to flush the command queue.")

    status = clEnqueueReadBuffer(cq, arg1, CL_TRUE, 0, sizeof(cl_float4)*values.size(), values.data(), 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to read buffer from the kernel.")

    print_matrix(values, mx_size);
}

void run_transpose_f1(const platform_device_t& pd, std::vector<float>&& values)
{
    cl_context cxt = nullptr;
    cl_command_queue cq = nullptr;
    cl_mem arg1 = nullptr;
    cl_mem arg3 = nullptr;

    scope_end_executor se_exec(
        [&]()
        {
            if (arg1)
                clReleaseMemObject(arg1);

            if (arg3)
                clReleaseMemObject(arg3);

            if (cq)
                clReleaseCommandQueue(cq);

            if (cxt)
                clReleaseContext(cxt);
        }
    );

    const cl_context_properties cxt_props[] = {
        CL_CONTEXT_PLATFORM, reinterpret_cast<cl_context_properties>(pd.platform),
        0
    };

    cl_int status = CL_SUCCESS;
    cxt = clCreateContext(cxt_props, 1, &pd.device, nullptr, nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create context.")

    // use deprecated function here to avoid crash.
    cq = clCreateCommandQueue(cxt, pd.device, 0, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create command queue")

    program_module pm(pd.device, cxt, ks_transpose, ks_transpose_n);
    cl_kernel ktranspose = pm.get_kernel("transpose_f1");
    if (!ktranspose)
        throw std::runtime_error("failed to create kernel for 'transpose");

    size_t max_wg_size = pm.get_max_work_group_size(ktranspose);
    cl_uint mx_size = static_cast<cl_uint>(std::sqrt(values.size()));
    size_t wi_size = mx_size * mx_size;
    if (wi_size != values.size())
        throw std::runtime_error("invalid matrix size!");

    size_t wg_size = std::min(max_wg_size, wi_size);

    std::cout << "matrix size: " << mx_size << " x " << mx_size << std::endl;
    std::cout << "max work group size: " << max_wg_size << std::endl;

    std::cout << "number of values: " << values.size() << std::endl;

    print_matrix(values, mx_size);

    arg1 = clCreateBuffer(cxt, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(float)*values.size(), values.data(), &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg1.")

    status = clSetKernelArg(ktranspose, 0, sizeof(arg1), &arg1);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 1.")

    status = clSetKernelArg(ktranspose, 1, sizeof(mx_size), &mx_size);
    CHECK_STATUS_OR_THROW(status, "failed to set argument 2.")

    // Dispatch the kernel.
    std::cout << "work item size: " << wi_size << "; work group size: " << wg_size << std::endl;

    status = clEnqueueNDRangeKernel(cq, ktranspose, 1, nullptr, &wi_size, &wg_size, 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to disptach the kernel.")

    status = clFlush(cq);
    CHECK_STATUS_OR_THROW(status, "failed to flush the command queue.")

    status = clEnqueueReadBuffer(cq, arg1, CL_TRUE, 0, sizeof(float)*values.size(), values.data(), 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to read buffer from the kernel.")

    print_matrix(values, mx_size);
}

int main(int argc, const char** argv)
{
    size_t mx_size = 64; // 64 by 64 matrix by default.

    if (argc > 1)
    {
        char* pend = nullptr;
        size_t user_mx_size = std::strtoul(argv[1], &pend, 10);
        if (pend && argv[1] < pend)
            // this is a valid unsigned integer.
            mx_size = user_mx_size;
    }

    std::vector<float> mxdata(mx_size*mx_size, 0.0f);
    size_t i = 0;
    std::for_each(mxdata.begin(), mxdata.end(), [&i](float& v) { v = i++; });

    try
    {
        platform_device_t device_selected = pick_device();
        run_transpose_f1(device_selected, std::move(mxdata));
    }
    catch (const std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
