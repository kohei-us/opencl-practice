
#include "opencl.hpp"

#include <vector>
#include <iostream>
#include <sstream>
#include <map>
#include <algorithm>

namespace {

std::vector<platform_device_t> get_all_devices()
{
    std::vector<platform_device_t> devices;

    cl_uint platformCount = 0;
    cl_int status;

    status = clGetPlatformIDs(0, nullptr, &platformCount);
    CHECK_STATUS_OR_THROW(status, "failed to obtain the platform ID count.")

    std::vector<cl_platform_id> platformIDs(platformCount);
    status = clGetPlatformIDs(platformCount, platformIDs.data(), nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to obtains a list of platform IDs.")

    for (const cl_platform_id pid : platformIDs)
    {
        platform_device_t pd;
        pd.platform = pid;

        cl_uint deviceCount = 0;
        status = clGetDeviceIDs(pid, CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceCount);
        CHECK_STATUS_OR_THROW(status, "failed to obtain the device ID count.")

        std::vector<cl_device_id> deviceIDs(deviceCount);
        status = clGetDeviceIDs(pid, CL_DEVICE_TYPE_ALL, deviceCount, deviceIDs.data(), nullptr);
        CHECK_STATUS_OR_THROW(status, "failed to obtain a list of device IDs.")

        for (const cl_device_id did : deviceIDs)
        {
            pd.device = did;
            devices.push_back(pd);
        }
    }

    return devices;
}

} // anonymous namespace

platform_device_t pick_device()
{
    std::vector<platform_device_t> devices = get_all_devices();
    cl_int status = CL_SUCCESS;

    std::cout << "Please select a device from the list." << std::endl;

    size_t pos = 0;

    for (platform_device_t device : devices)
    {
        // Get the device name to display in the terminal.
        size_t n = 0;
        std::string buf(255, '\0');
        status = clGetDeviceInfo(device.device, CL_DEVICE_NAME, buf.size(), (void*)buf.data(), &n);
        CHECK_STATUS_OR_THROW(status, "failed to query device name.")
        buf.resize(n, '\0');
        std::cout << (pos+1) << ": " << buf << std::endl;

        ++pos;
    }

    std::cout << std::endl << "selection: ";

    // TODO : obviously this only works up to 9 devices.
    char c;
    std::cin >> c;

    if (c < '1' || '9' < c)
        throw std::runtime_error("input out-of-range.");

    pos = c - '1';

    if (pos >= devices.size())
        throw std::runtime_error("input out-of-range.");

    return devices.at(pos);
}

struct program_module::impl
{
    using kernels_type = std::map<std::string, cl_kernel>;

    cl_program m_program = nullptr;
    cl_device_id m_device = nullptr;

    kernels_type m_kernels;

    ~impl()
    {
        if (m_program)
            clReleaseProgram(m_program);

        for (kernels_type::value_type& v : m_kernels)
        {
            cl_kernel k = v.second;
            clReleaseKernel(k);
        }
    }
};

program_module::program_module(cl_device_id device, cl_context cxt, const char* src, size_t src_n) :
    mp_impl(std::make_unique<impl>())
{
    mp_impl->m_device = device;

    cl_int status = 0;
    mp_impl->m_program = clCreateProgramWithSource(cxt, 1, &src, &src_n, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create program")

    const char* build_options = "-cl-mad-enable -cl-no-signed-zeros";
    status = clBuildProgram(mp_impl->m_program, 1, &device, build_options, nullptr, nullptr);

    if (status != CL_SUCCESS)
    {
        // Fetch the compiler error message.

        // First, query the size of the log string.
        size_t n = 0;
        status = clGetProgramBuildInfo(
            mp_impl->m_program, device, CL_PROGRAM_BUILD_LOG, 0, nullptr, &n);

        // Now fetch the log string for real.
        std::string buf(n, '\0');
        status = clGetProgramBuildInfo(
            mp_impl->m_program, device, CL_PROGRAM_BUILD_LOG, buf.size(), (void*)buf.data(), nullptr);

        CHECK_STATUS_OR_THROW(status, "failed to fetch the compiler error message.")

        throw std::runtime_error(buf);
    }
}

program_module::~program_module() {}

cl_kernel program_module::get_kernel(const std::string& name)
{
    if (!mp_impl->m_program)
        return nullptr;

    {
        // See if the same kernel already exists.
        auto it = mp_impl->m_kernels.find(name);
        if (it != mp_impl->m_kernels.end())
            return it->second;
    }

    cl_int status = 0;
    cl_kernel kernel = clCreateKernel(mp_impl->m_program, name.data(), &status);
    if (status != CL_SUCCESS)
        return nullptr;

    // Store the kernel pointer for later cleanup.
    auto it = mp_impl->m_kernels.insert(
        impl::kernels_type::value_type(name, kernel));

    if (!it.second)
        throw std::runtime_error("failed to insert a kernel.");

    return kernel;
}

size_t program_module::get_max_work_group_size() const
{
    size_t max_wg_size = 0;

    for (const impl::kernels_type::value_type& v : mp_impl->m_kernels)
    {
        size_t n = get_max_work_group_size(v.second);
        if (max_wg_size)
            max_wg_size = std::min(n, max_wg_size);
        else
            max_wg_size = n;
    }

    return max_wg_size;
}

size_t program_module::get_max_work_group_size(cl_kernel kernel) const
{
    size_t max_wg_size = 0;
    cl_int status = clGetKernelWorkGroupInfo(
      kernel, mp_impl->m_device, CL_KERNEL_WORK_GROUP_SIZE, sizeof(max_wg_size), &max_wg_size, nullptr);

    if (status != CL_SUCCESS)
        max_wg_size = 0;

    return max_wg_size;
}
