#include "util.hpp"
#include "opencl.hpp"

#include <vector>
#include <iostream>
#include <ostream>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <thread>
#include <future>
#include <chrono>
#include <queue>
#include <limits>
#include <numeric>

#include <boost/program_options.hpp>
#include <mdds/sorted_string_map.hpp>
#include <mdds/global.hpp>

using uint16_sort_values_type = std::vector<uint16_t>;
using int16_sort_values_type = std::vector<int16_t>;
using uint32_sort_values_type = std::vector<uint32_t>;
using int32_sort_values_type = std::vector<int32_t>;
using float_sort_values_type = std::vector<float>;
using double_sort_values_type = std::vector<double>;

using future_type = std::future<void>;

const char* ks_radix_sort =

#include "kernel/radix_sort.cl.inl"

;

size_t ks_radix_sort_n = strlen(ks_radix_sort);

enum class array_value_t
{
    unspecified = 0,
    value_float,
    value_double,
    value_int16,
    value_int32,
    value_int64,
    value_uint16,
    value_uint32,
    value_uint64
};

enum class sort_algorithm_t
{
    unspecified = 0,
    radix_gpu,
    radix_single_threaded,
    radix_multi_threaded,
    std_sort
};

template<typename ValueT>
struct uint_type
{
    typedef ValueT type;
};

template<>
struct uint_type<int16_t>
{
    typedef uint16_t type;
};

template<>
struct uint_type<int32_t>
{
    typedef uint32_t type;
};

template<>
struct uint_type<float>
{
    typedef uint32_t type;
};

template<>
struct uint_type<double>
{
    typedef uint64_t type;
};

struct offset_item
{
    size_t counter = 0u;
    size_t offset = 0u;
};

struct thread_item
{
    size_t index = 0u;
    size_t offset = 0u;
    size_t size = 0u;
};

inline void reset_elements(std::vector<offset_item>& values)
{
    size_t n = sizeof(offset_item) * values.size();
    std::memset(values.data(), 0u, n);
}

void block_on_all_futures(std::queue<future_type>& futures)
{
    // Block until all the threads are complete.
    while (!futures.empty())
    {
        future_type f = std::move(futures.front());
        futures.pop();
        f.wait();
    }
}

std::vector<thread_item> create_thread_items(size_t data_size, uint16_t thread_count)
{
    std::vector<thread_item> thread_items(thread_count);

    size_t size_per_thread = data_size / thread_count;
    size_t offset = 0;
    for (size_t i = 0; i < thread_items.size() - 1; ++i, offset += size_per_thread)
    {
        thread_item& ti = thread_items[i];
        ti.index = i;
        ti.offset = offset;
        ti.size = size_per_thread;
    }

    thread_item& ti = thread_items.back();
    ti.index = thread_items.size() - 1;
    ti.offset = offset;
    ti.size = data_size - offset;

    return thread_items;
}

template<typename ValuesT>
void iterate_by_thread_and_byte(
    const ValuesT& data, const thread_item& ti, const size_t byte_pos,
    const typename uint_type<typename ValuesT::value_type>::type mask,
    std::function<void(size_t, typename ValuesT::value_type)> mutator)
{
    using value_type = typename ValuesT::value_type;
    using byte_type = typename uint_type<value_type>::type;

    auto i = data.begin();
    std::advance(i, ti.offset);
    auto ie = i;
    std::advance(ie, ti.size);

    // Offset in the offset table to the first byte for the current thread.
    const size_t bv_offset = 256u * ti.index;

    std::for_each(i, ie,
        [&](const value_type v)
        {
            byte_type bv = (mask & v);
            bv >>= 8*byte_pos;
            mutator(bv_offset+bv, v);
        }
    );
}

template<typename F, typename UIntT>
UIntT get_masked_bytes(F v, UIntT mask)
{
    UIntT bytes;
    std::memcpy(&bytes, &v, sizeof(F));
    return (mask & bytes);
}

template<typename FValuesT, typename FFuncT>
void cpu_radix_sort_float(FValuesT& data, FFuncT func)
{
    using value_type = typename FValuesT::value_type;
    using byte_type = typename uint_type<value_type>::type;

    byte_type mask = 0xFF;
    const size_t byte_size = sizeof(value_type);

    std::vector<offset_item> offsets(256u);

    for (size_t byte = 0; byte < byte_size-1; ++byte, mask <<= 8)
    {
        // Do the counting in the first pass.
        func.count_values(data, mask, byte, offsets);

        // Build the offset table.

        FValuesT sorted(data.size(), 0);

        // Initialize the memory offsets with the offset to the first element.
        std::vector<value_type*> mem_offsets(256u, sorted.data());

        offsets[0].offset = 0;

        for (size_t i = 1; i < offsets.size(); ++i)
        {
            offsets[i].offset = offsets[i-1].offset + offsets[i-1].counter;
            mem_offsets[i] += offsets[i].offset;
        }

        for (const value_type v : data)
        {
            byte_type bv = get_masked_bytes(v, mask);
            bv >>= 8*byte;
            value_type* p = mem_offsets[bv]++;
            *p = v;
        }

        data.swap(sorted);

        reset_elements(offsets);
    }

    {
        // last iteration handles the sign bit.

        // Do the counting in the first pass.
        size_t negative_values = func.count_values_msb(data, mask, byte_size-1, offsets);

        // Build the offset table.

        FValuesT sorted(data.size(), 0);

        // Initialize the memory offsets with the offset to the first element.
        std::vector<value_type*> mem_offsets(256u, sorted.data()+negative_values);

        offsets[0u].offset = 0;
        const size_t offsets_half_size = offsets.size()/2;

        for (size_t i = 1u; i < offsets_half_size; ++i)
        {
            offsets[i].offset = offsets[i-1].offset + offsets[i-1].counter;
            mem_offsets[i] += offsets[i].offset;
        }

        // We move left from the "center" position for offsets for the
        // negative values, and unlike unsigned integer values, we move from
        // right to left in each byte-value segment.

        offsets[offsets_half_size].offset = 1;
        mem_offsets[offsets_half_size] -= offsets[offsets_half_size].offset;

        for (size_t i = 129u; i < offsets.size(); ++i)
        {
            offsets[i].offset = offsets[i-1].offset + offsets[i-1].counter;
            mem_offsets[i] -= offsets[i].offset;
        }

        for (const value_type v : data)
        {
            byte_type bv = get_masked_bytes(v, mask);
            bv >>= 8*(byte_size-1);
            value_type* p = (bv & 0x80) ? mem_offsets[bv]-- : mem_offsets[bv]++;
            *p = v;
        }

        data.swap(sorted);
    }
}

template<typename FValuesT>
struct float_func_single_thread
{
    using value_type = typename FValuesT::value_type;
    using byte_type = typename uint_type<value_type>::type;

    const size_t thread_count = 1;

    void count_values(
        const FValuesT& data, byte_type mask, size_t byte_pos, std::vector<offset_item>& offsets) const
    {
        for (const value_type v : data)
        {
            byte_type bv = get_masked_bytes(v, mask);
            bv >>= 8*byte_pos;
            ++offsets[bv].counter;
        }
    }

    size_t count_values_msb(
        const FValuesT& data, byte_type mask, size_t byte_pos, std::vector<offset_item>& offsets) const
    {
        size_t negative_values = 0;

        for (const value_type v : data)
        {
            byte_type bv = get_masked_bytes(v, mask);
            bv >>= 8*byte_pos;
            ++offsets[bv].counter;

            if (bv & 0x80)
                ++negative_values;
        }

        return negative_values;
    }
};

template<typename ValuesT>
void cpu_radix_sort_int(ValuesT& data)
{
    using value_type = typename ValuesT::value_type;

    value_type mask = 0xFF;
    const size_t byte_size = sizeof(value_type);

    std::vector<offset_item> offsets(256u);

    for (size_t byte = 0; byte < byte_size-1; ++byte, mask <<= 8)
    {
        // Do the counting in the first pass.
        for (const value_type v : data)
        {
            typename std::make_unsigned<value_type>::type bv = (mask & v);
            bv >>= 8*byte;
            ++offsets[bv].counter;
        }

        // Build the offset table.

        ValuesT sorted(data.size(), 0);

        // Initialize the memory offsets with the offset to the first element.
        std::vector<value_type*> mem_offsets(256u, sorted.data());

        offsets[0].offset = 0;

        for (size_t i = 1; i < 256u; ++i)
        {
            offsets[i].offset = offsets[i-1].offset + offsets[i-1].counter;
            mem_offsets[i] += offsets[i].offset;
        }

        for (const value_type v : data)
        {
            typename std::make_unsigned<value_type>::type bv = (mask & v);
            bv >>= 8*byte;
            value_type* p = mem_offsets[bv]++;
            *p = v;
        }

        data.swap(sorted);

        reset_elements(offsets);
    }

    {
        // last iteration handles the sign bit.

        size_t negative_values = 0;

        // Do the counting in the first pass.
        for (const value_type v : data)
        {
            typename std::make_unsigned<value_type>::type bv = (mask & v);
            bv >>= 8*(byte_size-1);
            ++offsets[bv].counter;

            if (bv & 0x80)
                ++negative_values;
        }

        // Build the offset table.

        ValuesT sorted(data.size(), 0);

        // Initialize the memory offsets with the offset to the first element.
        std::vector<value_type*> mem_offsets(256u, sorted.data()+negative_values);

        offsets[0u].offset = 0;

        for (size_t i = 1u; i < 128u; ++i)
        {
            offsets[i].offset = offsets[i-1].offset + offsets[i-1].counter;
            mem_offsets[i] += offsets[i].offset;
        }

        // We move left from the "center" position for offsets for the
        // negative values, but note that within each byte-value segment, the
        // order of negative values still flows from left to right.

        offsets[255u].offset = offsets[255u].counter;
        mem_offsets[255u] -= offsets[255u].offset;

        for (size_t i = 254u; i >= 128u; --i)
        {
            offsets[i].offset = offsets[i+1].offset + offsets[i].counter;
            mem_offsets[i] -= offsets[i].offset;
        }

        for (const value_type v : data)
        {
            typename std::make_unsigned<value_type>::type bv = (mask & v);
            bv >>= 8*(byte_size-1);
            value_type* p = mem_offsets[bv]++;
            *p = v;
        }

        data.swap(sorted);
    }
}

template<typename ValuesT>
void cpu_radix_sort_uint(ValuesT& data)
{
    using value_type = typename ValuesT::value_type;

    value_type mask = 0xFF;
    const size_t byte_size = sizeof(value_type);

    std::vector<offset_item> offsets(256u);

    for (size_t byte = 0; byte < byte_size; ++byte, mask <<= 8)
    {
        // Do the counting in the first pass.
        for (const value_type v : data)
        {
            value_type bv = (mask & v);
            bv >>= 8*byte;
            ++offsets[bv].counter;
        }

        // Build the offset table.

        ValuesT sorted(data.size(), 0);

        std::vector<value_type*> mem_offsets(256u, nullptr);
        value_type* p = data.data(); // point to the current memory offset.

        for (size_t byte_pos = 0; byte_pos < 256u; ++byte_pos)
        {
            mem_offsets[byte_pos] = p;
            p += offsets[byte_pos].counter;
        }

        for (const value_type v : data)
        {
            value_type bv = (mask & v);
            bv >>= 8*byte;
            value_type* p = mem_offsets[bv]++;
            *p = v;
        }

        data.swap(sorted);

        reset_elements(offsets);
    }
}

template<typename ValuesT>
void cpu_radix_sort_uint_threaded(ValuesT& data, const uint16_t thread_count)
{
    using value_type = typename ValuesT::value_type;

    value_type mask = 0xFF;
    const size_t byte_size = sizeof(value_type);

    // Slice the data by the number of threads to use.
    std::vector<thread_item> thread_items = create_thread_items(data.size(), thread_count);

    std::vector<offset_item> offsets(256u*thread_count);

    for (size_t byte = 0; byte < byte_size; ++byte, mask <<= 8)
    {
        std::queue<future_type> futures;

        // Do the counting in the first pass.  Launch child threads to handle
        // all segments except for the last.
        for (size_t thread_index = 0; thread_index < thread_count-1; ++thread_index)
        {
            future_type f = std::async(
                std::launch::async, iterate_by_thread_and_byte<ValuesT>,
                std::cref(data), std::cref(thread_items[thread_index]), byte, mask,
                [&](size_t i, value_type v)
                {
                    ++offsets[i].counter;
                }
            );
            futures.push(std::move(f));
        }

        // main thread handles the last segment.
        iterate_by_thread_and_byte<ValuesT>(
            data, thread_items.back(), byte, mask,
            [&](size_t i, value_type v)
            {
                ++offsets[i].counter;
            }
        );

        block_on_all_futures(futures);

        // Build the offset table.

        ValuesT sorted(data.size(), 0);

        // Initialize the memory offsets with the offset to the first element.
        std::vector<value_type*> mem_offsets(256u*thread_count, nullptr);

        value_type* p = data.data(); // point to the current memory offset.

        for (size_t byte_pos = 0; byte_pos < 256u; ++byte_pos)
        {
            for (size_t thread_index = 0; thread_index < thread_count; ++thread_index)
            {
                size_t i = 256u * thread_index + byte_pos;
                mem_offsets[i] = p;
                p += offsets[i].counter;
            }
        }

        // Launch child threads to handle all segments except for the last.
        for (size_t thread_index = 0; thread_index < thread_count-1; ++thread_index)
        {
            future_type f = std::async(
                std::launch::async, iterate_by_thread_and_byte<ValuesT>,
                std::cref(data), std::cref(thread_items[thread_index]), byte, mask,
                [&](size_t i, value_type v)
                {
                    value_type* p = mem_offsets[i]++;
                    *p = v;
                }
            );
            futures.push(std::move(f));
        }

        // main thread handles the last segment.
        iterate_by_thread_and_byte<ValuesT>(
            data, thread_items.back(), byte, mask,
            [&](size_t i, value_type v)
            {
                value_type* p = mem_offsets[i]++;
                *p = v;
            }
        );

        block_on_all_futures(futures);

        data.swap(sorted);
        reset_elements(offsets);
    }
}

#if 0 // unused but keep it for reference purposes.

void cpu_radix_sort_uint_perbit(uint16_sort_values_type& data)
{
    size_t sd = sizeof(uint16_sort_values_type::value_type);
    size_t bit_size = sd * 8;
    size_t n = data.size();
    std::vector<size_t> high_value_positions;
    high_value_positions.reserve(n);

    uint16_sort_values_type shuffled;
    shuffled.reserve(n);

    uint16_sort_values_type::value_type mask = 0x1;
    for (size_t bit = 0; bit < bit_size; ++bit, mask <<= 1)
    {
        shuffled.clear();
        high_value_positions.clear();

        for (size_t i = 0; i < n; ++i)
        {
            uint16_sort_values_type::value_type v = data[i];
            if (v & mask)
                high_value_positions.push_back(i);
            else
                shuffled.push_back(v);
        }

        for (const size_t pos : high_value_positions)
            shuffled.push_back(data[pos]);

        data.swap(shuffled);
    }
}

#endif

template<typename ValuesT>
void print_values(ValuesT* data)
{
    std::copy(
        data->begin(), data->end(),
        std::ostream_iterator<typename ValuesT::value_type>(std::cout, " "));
    std::cout << std::endl;
}

template<typename ValuesT>
bool verify_values(const ValuesT& data)
{
    if (data.empty())
        return true;

    auto it = data.cbegin();
    typename ValuesT::value_type prev = *it;
    for (; it != data.cend(); ++it)
    {
        if (prev > *it)
            return false;
        prev = *it;
    }
    return true;
}

template<typename ValuesT>
void verify_and_print(const ValuesT& data)
{
    time_tracker tt;
    if (verify_values(data))
        std::cout << "values successfully verified. (" << tt.get_duration() << " seconds)" << std::endl;
    else
        std::cout << "verification on the values failed. (" << tt.get_duration() << " seconds)" << std::endl;
}

template<typename ValuesT>
void sort_values_radix_gpu(ValuesT* data, bool verify)
{
    throw std::runtime_error("FIXME: not implemented yet");
}

template<>
void sort_values_radix_gpu<uint16_sort_values_type>(uint16_sort_values_type* data, bool verify)
{
    using values_type = uint16_sort_values_type;
    const cl_uchar byte_size = sizeof(values_type::value_type);
    size_t original_size = data->size();

    platform_device_t pd = pick_device();

    time_tracker tt;

    cl_context cxt = nullptr;
    cl_command_queue cq = nullptr;
    cl_mem arg_data = nullptr;
    cl_mem arg_counter = nullptr;
    cl_mem arg_mem_offsets = nullptr;
    cl_mem arg_sorted = nullptr;

    scope_end_executor se_exec(
        [&]()
        {
            if (arg_counter)
                clReleaseMemObject(arg_counter);

            if (arg_mem_offsets)
                clReleaseMemObject(arg_mem_offsets);

            if (arg_data)
                clReleaseMemObject(arg_data);

            if (arg_sorted)
                clReleaseMemObject(arg_sorted);

            if (cq)
                clReleaseCommandQueue(cq);

            if (cxt)
                clReleaseContext(cxt);
        }
    );

    const cl_context_properties cxt_props[] = {
        CL_CONTEXT_PLATFORM, reinterpret_cast<cl_context_properties>(pd.platform),
        0
    };

    cl_int status = CL_SUCCESS;
    cxt = clCreateContext(cxt_props, 1, &pd.device, nullptr, nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create context.")

    // use deprecated function here to avoid crash.
    cq = clCreateCommandQueue(cxt, pd.device, 0, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create command queue")

    program_module pm(pd.device, cxt, ks_radix_sort, ks_radix_sort_n);

    cl_kernel kernel_counter = pm.get_kernel("radix_sort_uint16_count");
    cl_kernel kernel_mem_offsets = pm.get_kernel("radix_sort_uint16_mem_offsets");
    cl_kernel kernel_sorted_pos = pm.get_kernel("radix_sort_uint16_sorted_pos");

    if (!kernel_counter || !kernel_mem_offsets || !kernel_sorted_pos)
        throw std::runtime_error("failed to create all required kernels.");

    size_t max_wg_size = pm.get_max_work_group_size();
    if (!max_wg_size)
        throw std::runtime_error("invalid max work-group size.");

    // Ensure that the data size is a multiple of the max work-group size.
    size_t modulo = data->size() % max_wg_size;
    if (modulo)
    {
        size_t pad_size = max_wg_size - modulo;
        if (pad_size)
        {
            size_t final_size = data->size() + pad_size;
            data->resize(final_size, std::numeric_limits<values_type::value_type>::max());
        }
    }

    // Number of segments the data gets split into.
    uint32_t segment_count = static_cast<uint32_t>(data->size() / max_wg_size);

    size_t bufsize = segment_count * 256;

    arg_counter = clCreateBuffer(
        cxt, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,
        sizeof(uint32_t)*bufsize, nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg_counter.")

    arg_mem_offsets = clCreateBuffer(
        cxt, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,
        sizeof(uint32_t)*bufsize, nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg_mem_offsets.")

    arg_data = clCreateBuffer(
        cxt, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
        sizeof(uint16_t) * data->size(), data->data(), &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg_data.")

    arg_sorted = clCreateBuffer(
        cxt, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,
        sizeof(uint16_t) * data->size(), nullptr, &status);
    CHECK_STATUS_OR_THROW(status, "failed to create buffer for arg_sorted.")

    uint16_t mask = 0x00FF;
    uint16_sort_values_type sorted(data->size(), 0u);

    for (cl_uchar byte_pos = 0; byte_pos < byte_size; ++byte_pos, mask <<= 8)
    {
        {
            // Launch the counter kernel.
            status = clSetKernelArg(kernel_counter, 0, sizeof(arg_data), &arg_data);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 0.")

            status = clSetKernelArg(kernel_counter, 1, sizeof(cl_uint)*256, nullptr);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 1.")

            status = clSetKernelArg(kernel_counter, 2, sizeof(cl_uchar), &byte_pos);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 2.")

            status = clSetKernelArg(kernel_counter, 3, sizeof(uint16_t), &mask);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 3.")

            status = clSetKernelArg(kernel_counter, 4, sizeof(arg_counter), &arg_counter);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 4.")

            size_t wi = data->size();
            size_t wg = max_wg_size;
            status = clEnqueueNDRangeKernel(cq, kernel_counter, 1, nullptr, &wi, &wg, 0, nullptr, nullptr);
            CHECK_STATUS_OR_THROW(status, "failed to disptach the kernel.")
        }

        {
            // Launch the mem_offset kernel.  This one only uses one work item.

            status = clSetKernelArg(kernel_mem_offsets, 0, sizeof(arg_counter), &arg_counter);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 0.")

            status = clSetKernelArg(kernel_mem_offsets, 1, sizeof(segment_count), &segment_count);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 1.")

            status = clSetKernelArg(kernel_mem_offsets, 2, sizeof(arg_mem_offsets), &arg_mem_offsets);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 2.")

            size_t wi = 1;
            size_t wg = 1;
            status = clEnqueueNDRangeKernel(cq, kernel_mem_offsets, 1, nullptr, &wi, &wg, 0, nullptr, nullptr);
            CHECK_STATUS_OR_THROW(status, "failed to disptach the kernel.")
        }

        {
            // Launch the sorted position kernel.

            status = clSetKernelArg(kernel_sorted_pos, 0, sizeof(arg_data), &arg_data);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 0.")

            status = clSetKernelArg(kernel_sorted_pos, 1, sizeof(arg_mem_offsets), &arg_mem_offsets);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 1.")

            uint32_t segment_size = static_cast<uint32_t>(max_wg_size);
            status = clSetKernelArg(kernel_sorted_pos, 2, sizeof(segment_size), &segment_size);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 2.")

            status = clSetKernelArg(kernel_sorted_pos, 3, sizeof(cl_uchar), &byte_pos);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 3.")

            status = clSetKernelArg(kernel_sorted_pos, 4, sizeof(uint16_t), &mask);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 4.")

            status = clSetKernelArg(kernel_sorted_pos, 5, sizeof(arg_sorted), &arg_sorted);
            CHECK_STATUS_OR_THROW(status, "failed to set argument 5.")

            size_t wi = segment_count;
            size_t wg = (wi < max_wg_size) ? wi : max_wg_size;

            status = clEnqueueNDRangeKernel(cq, kernel_sorted_pos, 1, nullptr, &wi, &wg, 0, nullptr, nullptr);
            CHECK_STATUS_OR_THROW(status, "failed to disptach the sorted-position kernel.")
        }

        std::swap(arg_data, arg_sorted);
    }

    status = clEnqueueReadBuffer(cq, arg_data, CL_TRUE, 0, sizeof(uint16_t) * data->size(), data->data(), 0, nullptr, nullptr);
    CHECK_STATUS_OR_THROW(status, "failed to enqueue buffer from the kernel.")

    if (data->size() > original_size)
        data->resize(original_size);

    std::cout << "sort time (in seconds): " << tt.get_duration() << std::endl;
    std::cout.flush();

    if (verify)
        verify_and_print(*data);
}

template<typename ValuesT>
void sort_values_radix_st(ValuesT* data, bool verify)
{
    {
        time_tracker tt;
        if (std::is_signed<typename ValuesT::value_type>::value)
            cpu_radix_sort_int(*data);
        else
            cpu_radix_sort_uint(*data);
        std::cout << "sort time (in seconds): " << tt.get_duration() << std::endl;
        std::cout.flush();
    }

    if (verify)
        verify_and_print(*data);
}

template<typename ValuesT>
void sort_values_radix_mt(ValuesT* data, bool verify, uint16_t thread_count)
{
    {
        time_tracker tt;
        if (std::is_signed<typename ValuesT::value_type>::value)
            throw std::runtime_error("FIXME: not implemented yet");
        else
            cpu_radix_sort_uint_threaded(*data, thread_count);
        std::cout << "sort time (in seconds): " << tt.get_duration() << std::endl;
        std::cout.flush();
    }

    if (verify)
        verify_and_print(*data);
}

template<>
void sort_values_radix_mt<float_sort_values_type>(float_sort_values_type* data, bool verify, uint16_t thread_count)
{
    throw std::runtime_error("FIXME: not implemented yet");
}

template<>
void sort_values_radix_mt<double_sort_values_type>(double_sort_values_type* data, bool verify, uint16_t thread_count)
{
    throw std::runtime_error("FIXME: not implemented yet");
}

template<typename FValuesT>
void sort_values_radix_st_float(FValuesT* data, bool verify)
{
    {
        time_tracker tt;
        cpu_radix_sort_float(*data, float_func_single_thread<FValuesT>());
        std::cout << "sort time (in seconds): " << tt.get_duration() << std::endl;
        std::cout.flush();
    }

    if (verify)
        verify_and_print(*data);
}

template<>
void sort_values_radix_st<float_sort_values_type>(float_sort_values_type* data, bool verify)
{
    sort_values_radix_st_float(data, verify);
}

template<>
void sort_values_radix_st<double_sort_values_type>(double_sort_values_type* data, bool verify)
{
    sort_values_radix_st_float(data, verify);
}

template<typename FValuesT>
void sort_values_std(FValuesT* data, bool verify)
{
    {
        time_tracker tt;
        std::sort(data->begin(), data->end());
        std::cout << "sort time (in seconds): " << tt.get_duration() << std::endl;
        std::cout.flush();
    }

    if (verify)
        verify_and_print(*data);
}

template<typename ValuesT>
void sort_values_by_algorithm(sort_algorithm_t sat, ValuesT* data, bool verify, uint16_t thread_count = 0)
{
    switch (sat)
    {
        case sort_algorithm_t::radix_gpu:
            sort_values_radix_gpu(data, verify);
            break;
        case sort_algorithm_t::radix_single_threaded:
            sort_values_radix_st(data, verify);
            break;
        case sort_algorithm_t::radix_multi_threaded:
            sort_values_radix_mt(data, verify, thread_count);
            break;
        case sort_algorithm_t::std_sort:
            sort_values_std(data, verify);
            break;
        default:
            std::cout << "sorting not performed." << std::endl;
    }
}

template<typename ValueT>
ValueT to_value(const std::string& s);

template<>
int16_t to_value<int16_t>(const std::string& s)
{
    return std::stoi(s);
}

template<>
uint16_t to_value<uint16_t>(const std::string& s)
{
    return static_cast<uint16_t>(std::stoul(s));
}

template<>
int32_t to_value<int32_t>(const std::string& s)
{
    return std::stoi(s);
}

template<>
uint32_t to_value<uint32_t>(const std::string& s)
{
    return std::stoul(s);
}

template<>
float to_value<float>(const std::string& s)
{
    return std::stof(s);
}

template<>
double to_value<double>(const std::string& s)
{
    return std::stod(s);
}

template<typename ValuesT>
ValuesT load_values_from_string(const std::string& content)
{
    time_tracker tt;
    ValuesT ret;

    const char* p = content.data();
    const char* p_end = p + content.size();

    const char* p0 = nullptr;
    for (; p != p_end; ++p)
    {
        switch (*p)
        {
            case '\r':
            case ' ':
            {
                // Ignore these characters.
                p0 = nullptr;
                continue;
            }
            case '\n':
            case ',':
            {
                // These are delimiters.
                if (p0)
                {
                    std::string s(p0, std::distance(p0,p));
                    ret.push_back(to_value<typename ValuesT::value_type>(s));
                }
                p0 = nullptr;
                break;
            }
            default:
            {
                if (!p0)
                    p0 = p;
            }
        }
    }

    std::cout << "load time (in seconds): " << tt.get_duration() << std::endl;
    std::cout << "values loaded: " << ret.size() << std::endl;
    std::cout.flush();

    return ret;
}

namespace array_value_type_map {

typedef mdds::sorted_string_map<array_value_t> map_type;

// Must be sorted by the keys.
const std::vector<map_type::entry> entries = {
    { MDDS_ASCII("double"), array_value_t::value_double },
    { MDDS_ASCII("float"),  array_value_t::value_float  },
    { MDDS_ASCII("int16"),  array_value_t::value_int16  },
    { MDDS_ASCII("int32"),  array_value_t::value_int32  },
    { MDDS_ASCII("int64"),  array_value_t::value_int64  },
    { MDDS_ASCII("uint16"), array_value_t::value_uint16 },
    { MDDS_ASCII("uint32"), array_value_t::value_uint32 },
    { MDDS_ASCII("uint64"), array_value_t::value_uint64 },
};

const map_type& get()
{
    static map_type mt(entries.data(), entries.size(), array_value_t::unspecified);
    return mt;
}

}

namespace sort_algorithm_type_map {

typedef mdds::sorted_string_map<sort_algorithm_t> map_type;

// Must be sorted by the keys.
const std::vector<map_type::entry> entries = {
    { MDDS_ASCII("radix-gpu"), sort_algorithm_t::radix_gpu             },
    { MDDS_ASCII("radix-mt"),  sort_algorithm_t::radix_multi_threaded  },
    { MDDS_ASCII("radix-st"),  sort_algorithm_t::radix_single_threaded },
    { MDDS_ASCII("std-sort"),  sort_algorithm_t::std_sort              },
};

const map_type& get()
{
    static map_type mt(entries.data(), entries.size(), sort_algorithm_t::unspecified);
    return mt;
}

}

int main(int argc, char** argv)
{
    namespace po = ::boost::program_options;

    po::options_description desc("Allowed options");
    desc.add_options()("help,h", "print this help.");
    desc.add_options()("type,t", po::value<std::string>(), "value type.");
    desc.add_options()("algorithm,a", po::value<std::string>(), "sort algorithm to use.");
    desc.add_options()("thread", po::value<uint16_t>(), "number of threads to use.");
    desc.add_options()("verify", "verify the sorted results.");

    po::options_description hidden("Hidden options");
    hidden.add_options()
        ("input-file", po::value<std::string>(), "input file");

    po::options_description cmd_opt;
    cmd_opt.add(desc).add(hidden);

    po::positional_options_description po_desc;
    po_desc.add("input-file", -1);

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).positional(po_desc).run(), vm);
        po::notify(vm);
    }
    catch (const std::exception& e)
    {
        // Unknown options.
        std::cout << e.what() << std::endl;
        std::cout << desc;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        std::cout << "Usage: radix-sort [options] FILE" << std::endl
            << std::endl
            << "The FILE must be a csv file containing numbers to sort." << std::endl << std::endl
            << desc;
        return EXIT_SUCCESS;
    }

    if (!vm.count("input-file"))
    {
        std::cerr << "no input file." << std::endl;
        return EXIT_FAILURE;
    }

    std::string filepath = vm["input-file"].as<std::string>();

    std::ifstream file(filepath.c_str());
    if (!file)
    {
        std::cerr << filepath << " is not a valid file." << std::endl;
        return EXIT_FAILURE;
    }

    if (!vm.count("type"))
    {
        std::cerr << "value type must be specified." << std::endl;
        return EXIT_FAILURE;
    }

    if (!vm.count("algorithm"))
    {
        std::cerr << "algortihm type must be specified." << std::endl;
        return EXIT_FAILURE;
    }

    uint16_t thread_count = vm.count("thread") > 0 ? vm["thread"].as<uint16_t>() : 0;

    std::cout << "thread count: " << int(thread_count) << std::endl;
    std::cout.flush();

    // Determine the value type specified.
    std::string s = vm["type"].as<std::string>();
    array_value_t avt = array_value_type_map::get().find(s.data(), s.size());

    // Determine the sort algorithm to use.
    s = vm["algorithm"].as<std::string>();
    sort_algorithm_t algorithm = sort_algorithm_type_map::get().find(s.data(), s.size());

    try
    {
        std::ostringstream os;
        os << file.rdbuf();
        file.close();

        bool verify = vm.count("verify") > 0;

        switch (avt)
        {
            case array_value_t::value_int16:
            {
                int16_sort_values_type values =
                    load_values_from_string<int16_sort_values_type>(os.str());
                sort_values_by_algorithm(algorithm, &values, verify);
                break;
            }
            case array_value_t::value_uint16:
            {
                uint16_sort_values_type values =
                    load_values_from_string<uint16_sort_values_type>(os.str());
                sort_values_by_algorithm(algorithm, &values, verify, thread_count);
                break;
            }
            case array_value_t::value_float:
            {
                float_sort_values_type values =
                    load_values_from_string<float_sort_values_type>(os.str());
                sort_values_by_algorithm(algorithm, &values, verify);
                break;
            }
            case array_value_t::value_double:
            {
                double_sort_values_type values =
                    load_values_from_string<double_sort_values_type>(os.str());
                sort_values_by_algorithm(algorithm, &values, verify);
                break;
            }
            case array_value_t::value_int32:
            {
                int32_sort_values_type values =
                    load_values_from_string<int32_sort_values_type>(os.str());
                sort_values_by_algorithm(algorithm, &values, verify);
                break;
            }
            case array_value_t::value_uint32:
            {
                uint32_sort_values_type values =
                    load_values_from_string<uint32_sort_values_type>(os.str());
                sort_values_by_algorithm(algorithm, &values, verify, thread_count);
                break;
            }
            case array_value_t::value_int64:
            case array_value_t::value_uint64:
                std::cerr << "value type not implemented yet: " << vm["type"].as<std::string>() << std::endl;
                return EXIT_FAILURE;
                break;
            case array_value_t::unspecified:
            default:
                std::cerr << "unknown value type specified: " << vm["type"].as<std::string>() << std::endl;
                return EXIT_FAILURE;
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << "unknown error occurred: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
