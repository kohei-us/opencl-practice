#!/usr/bin/env python3

import argparse


def write_numbers(writable, size, integer, signed):
    if size <= 0:
        # nothing to write.
        return

    import random
    if integer:
        if signed:
            def _gen():
                return random.randint(-5000, 5000)
        else:
            def _gen():
                return random.randint(0, 10000)
    else:
        def _gen():
            return random.uniform(-5000, 5000)

    max_colsize = 512
    writable.write(str(_gen()))
    colsize = 1

    for i in range(size-1):
        if colsize == max_colsize:
            writable.write('\n')
            colsize = 0
        else:
            writable.write(',')
        v = _gen()
        writable.write(str(v))
        colsize += 1

    writable.write('\n')


def main():
    parser = argparse.ArgumentParser(
        description="Script to generate random set of numbers in csv format.")
    parser.add_argument("outfile", metavar="FILE", nargs=1,
                        help="output csv file to write the numbers to.")
    parser.add_argument("--size", type=int, help="Number of values to generate.")
    parser.add_argument("--int", dest="integer", action="store_true")
    parser.add_argument("--unsigned", dest="signed", action="store_false")
    parser.set_defaults(integer=False, signed=True)

    args = parser.parse_args()

    with open(args.outfile[0], 'w') as f:
        write_numbers(f, args.size, integer=args.integer, signed=args.signed)


if __name__ == "__main__":
    main()
