#!/usr/bin/env python3

import argparse
import os.path
import os


def escape_line(line):
    escaped_line = ""
    for c in line:
        if c == '"':
            escaped_line += '\\"'
        elif c == '\\':
            escaped_line += '\\\\'
        else:
            escaped_line += c
    return escaped_line


def process_kernel_path(path):
    dirpath = os.path.abspath(path)

    if not os.path.isdir(dirpath):
        # this must be a directory.
        return

    # find all .cl files in this path.
    for filename in os.listdir(dirpath):
        filepath = "{}/{}".format(dirpath, filename)
        if os.path.splitext(filepath)[1] != ".cl":
            continue

        print(filepath)
        with open(filepath, 'r') as infile:
            out_filepath = "{}.inl".format(filepath)
            with open(out_filepath, 'w') as outfile:
                for line in infile.readlines():
                    line = escape_line(line.rstrip())
                    outfile.write('"')
                    outfile.write(line)
                    outfile.write('\\n"\n')



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("path", nargs="+")
    args = parser.parse_args()
    for path in args.path:
        process_kernel_path(path)


if __name__ == "__main__":
    main()

