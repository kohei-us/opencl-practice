
CPPFLAGS= \
	-I$(AMDAPPSDKROOT)/include \
	-I../mdds/include \
	-I./include \
	-std=c++14 -O2 -Wno-deprecated-declarations -pthread
LDFLAGS=-lOpenCL -lboost_program_options -pthread

ROOTDIR=.
SRCDIR=$(ROOTDIR)/src
KERNELDIR=$(SRCDIR)/kernel

EXEC=\
	query-device \
	sum \
	radix-sort

all: $(EXEC)

pre:
	@exec $(ROOTDIR)/bin/make-inline-kernel.py $(KERNELDIR)

query-device: $(SRCDIR)/query_device.cpp
	$(CXX) $(CPPFLAGS) -o $@ $< $(LDFLAGS)

sum: pre $(SRCDIR)/sum.cpp
	$(CXX) $(CPPFLAGS) -o $@ $(SRCDIR)/sum.cpp $(LDFLAGS)

radix-sort: $(SRCDIR)/radix_sort.cpp $(SRCDIR)/util.cpp
	$(CXX) $(CPPFLAGS) -o $@ $(SRCDIR)/radix_sort.cpp $(SRCDIR)/util.cpp $(LDFLAGS)

clean:
	rm -f $(EXEC) $(KERNELDIR)/*.inl

.PHONY: pre clean
